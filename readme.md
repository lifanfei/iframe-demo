### 安装服务
```
npm install http-server
```
## 演示 父子界面-在跨域情况下-消息传递
### 启动服务A:8080
```
cd siteA & http-server -p 8080 -o parent.html
```
### 启动服务B:8081
```
cd siteB & http-server -p 8081 -o child.html
```

## 演示 子页面发起请求-在跨域情况下-让父页面关闭子页面
### 启动服务A:8080
```
cd siteA & http-server -p 8080 -o a.html
```
### 启动服务B:8081
```
cd siteB & http-server -p 8081 -o b.html
```

## 演示 父子界面-非跨域情况下-消息传递
### 启动服务A:8080
```
cd iframe-demo & http-server -p 8080 -o ./siteA/one.html --cors
```
