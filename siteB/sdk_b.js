(function () {
  var sdk = window.SDK || {};
  sdk.uname = null;
  //发送
  sdk.getUname = function () {
    window.top.postMessage(
      {
        action: "getUname",
      },
      "*"
    );
  };
  sdk.sayHi = function (info) {
    window.top.postMessage(
      {
        action: "sayHi",
        info: {
          msg: info.msg,
        },
      },
      "*"
    );
  };
  sdk.colseChildIframe = function (data) {
    window.top.postMessage(
      {
        action: "colseChildIframe",
        data: data,
      },
      "*"
    );
  };
  //接收
  window.addEventListener("message", function (e) {
    var res = e;
    var action = res.data.action;
    var info = res.data.info;
    //判断域名
    if (res.origin == "http://127.0.0.1:8080") {
      switch (action) {
        case "getUname":
          sdk.uname = info;
          break;
        default:
          return;
      }
    }
  });
  //写入window
  window.SDK = sdk;
})();
